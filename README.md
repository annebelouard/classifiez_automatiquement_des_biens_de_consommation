# **Classification Automatique des Articles pour "Place de marché"**


**Description du Projet**  :  
Ce projet vise à développer un moteur de classification automatique d’articles pour une marketplace e-commerce nommée "Place de marché". En utilisant les descriptions textuelles et les images des produits, ce système automatise l'attribution de catégories aux articles, améliorant ainsi l'expérience utilisateur pour les vendeurs et les acheteurs.

